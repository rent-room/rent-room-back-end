package controllers

import (
	"net/http"
	"rent-room/models"
	"rent-room/utils/token"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type registerInput struct {
	Username string `json:"username" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type LoginInput struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// Register godoc
// @Summary Register New User.
// @Description Registering a new User.
// @Tags Auth
// @Param Body body registerInput true "the body to Register a new User"
// @Produce json
// @Success 200 {object} models.User
// @Router /register [post]
func RegisterUser(c *gin.Context) {
	// Validate input
	var input registerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": err.Error()})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": err.Error()})
		return
	}

	// Create User
	user := models.User{
		Email:    input.Email,
		Password: string(hashedPassword),
		Role:     "guest",
	}

	detail := models.UserDetail{
		FirstName: input.Username,
	}

	responData := map[string]string{
		"username": input.Username,
		"email":    input.Email,
	}

	db := c.MustGet("db").(*gorm.DB)
	db.Transaction(func(tx *gorm.DB) error {
		//register user
		if err := db.Create(&user).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": err.Error()})
			return err
		}

		// create user detail
		detail.UserID = &user.ID
		if err := db.Create(&detail).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": err.Error()})
			return err
		}
		return nil
	})

	c.JSON(http.StatusOK, gin.H{"success": true, "data": responData})
}

// LoginUser godoc
// @Summary Login User.
// @Description Logging in to get jwt token to access user api.
// @Tags Auth
// @Param Body body LoginInput true "the body to login"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func LoginUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": err.Error()})
		return
	}

	u := models.User{}

	// check email
	err := db.Model(models.User{}).Where("email = ?", input.Email).Preload("UserDetail").First(&u).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "email or password is incorrect."})
		return
	}

	// check password
	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(input.Password))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "email or password is incorrect."})
		return
	}

	token, err := token.GenerateToken(u.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": "internal server error."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "email": input.Email, "user_detail": u.UserDetail, "token": token})
}
