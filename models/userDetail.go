package models

import "time"

type UserDetail struct {
	ID              uint      `json:"user_detail_id" gorm:"primary_key"`
	FirstName       string    `gorm:"not null" json:"first_name"`
	LastName        string    `json:"last_name"`
	PhoneNumber     string    `json:"phone_number"`
	Address         string    `json:"address"`
	City            string    `json:"city"`
	Province        string    `json:"province"`
	ProfileImageURL string    `json:"profile_image_url"`
	CreatedAt       time.Time `json:"created_at"`
	UpdateAt        time.Time `json:"updated_at"`
	UserID          *uint     `json:"user_id"`
	// migration relation
	User User `gorm:"foreignkey:UserID"`
}
