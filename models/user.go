package models

import "time"

type User struct {
	ID        uint      `json:"user_id" gorm:"primary_key"`
	Email     string    `gorm:"not null;unique" json:"email"`
	Password  string    `gorm:"not null" json:"password"`
	Role      string    `gorm:"not null" json:"role"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	// migration relation
	UserDetail []UserDetail
}
