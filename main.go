package main

import (
	"log"
	"rent-room/config"
	"rent-room/docs"

	"rent-room/routes"
	"rent-room/utils"

	"github.com/joho/godotenv"
)

func main() {

	//programmatically set swagger info
	docs.SwaggerInfo.Title = "Rent Room Documentation API"
	docs.SwaggerInfo.Description = "This is Documention for Rent Room REST API ."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = utils.Getenv("SWAGGER_HOST", "localhost:8080")
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	environment := utils.Getenv("ENVIRONMENT", "development")

	if environment == "development" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}
